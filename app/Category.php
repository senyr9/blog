<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * Get the post that owns the comment.
     */
    public function posts()
    {
        return $this->hasMany('App\Post');
    }
}
