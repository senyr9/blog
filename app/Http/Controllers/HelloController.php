<?php

namespace App\Http\Controllers;

use http\Env\Response;
use http\Url;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class HelloController extends Controller
{
    public function __construct()
    {
        $this->middleware("Ip") ;
    }

    public function index($student){
        $students = ['Alassane', 'Cheikh', 'Penda'] ;

        array_push($students, $student);

        //return Response()->json($students)

        return view('hello.index', compact("students")) ;
    }

    public function hello(){

        $students = ['Alassane', "Cheikh", "Penda"] ;
        return view('hello.index',compact("students")) ;
    }
}
