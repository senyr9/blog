<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function home(){
        $recent_posts = Post::orderBy('id', 'desc')->limit(5)->get();
        return view('home.index', compact('recent_posts'));
    }

    public function detail($slug){
        $post = Post::where('slug', $slug)->first() ;
        return view("home.detail", compact('post'));
    }
}
