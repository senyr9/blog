<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PostController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all() ;

        return view('posts.index', compact("posts"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();

        $imageName = time().'.'.request()->media->getClientOriginalExtension();
        request()->media->move(public_path('images'), $imageName);

        $inputs['media'] = $imageName;
        $inputs['slug'] = Str::slug($request->title);
        $inputs['user_id'] = Auth::user()->id;

        if ($request->status == 'on'){

            $inputs['status'] = 1;
        }else{

            $inputs['status'] = 0;
        }

        unset($inputs["_token"]);

        Post::create($inputs) ;

        return redirect(route('posts.index'))->with('success', 'Votre article a été bien ajouté');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){

        $post = Post::find($id);

//        dd($post);
        return view('posts.edit', compact(['post']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        dd($request->all());

        $post = Post::find($id);

        $inputs = $request->all();

        if(file_exists($request->file('media'))){
//            dd($request->file('media'));
            $imageName = time().'.'.request()->media->getClientOriginalExtension();
            request()->media->move(public_path('images'), $imageName);

            $inputs['media'] = $imageName;
        }

        unset($inputs['_token']);


        $inputs['slug'] = Str::slug($inputs["title"]);

        if ($request->status == 'on'){

            $inputs['status'] = 1;
        }else{

            $inputs['status'] = 0;
        }

        $post->title = $inputs['title'];
        $post->content = $inputs['content'];
        $post->slug = $inputs['slug'];
        $post->date_publication = $post->date_publication;
        $post->status = $inputs['status'];

//        dd($inputs);

        $post->save();

        return redirect(route("posts.index"));



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->status = 0 ;
        $post->save() ;

        return redirect(route("posts.index"))->with('success', "Article non trouvé");

        if($post){
            $post->delete();
            return redirect(route("posts.index"))->with('success', "Article supprimé");
        }
    }
}
