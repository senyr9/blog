<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=10; $i++){
            DB::table('posts')->insert([
               'title' => "Article " . $i,
                'content' => 'Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem lorem Lorem lorem lorem lorem',
                'slug' => "Article-" . $i,
                'date_publication' => date('Y-m-d H:i:s'),
                'status' => true,
                'media' => "https://www.parkingdakar.com/storage/app/public/uploads/annonces/0a3028979f203e0b2ca1b5701fa9c59c.jpg",
                'user_id' => 1
            ]);
        }
    }
}
