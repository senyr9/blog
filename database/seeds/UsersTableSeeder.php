<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("users")->insert([
            'name' => "seny",
            'role' => "user",
            'email' => 'senyr@gmail.com',
            'username' => 'senyr@gmail.com',
            'password' => bcrypt('passer13'),
        ]);

        DB::table("users")->insert([
            'name' => "elhadji",
            'role' => "user",
            'email' => 'elhadji@gmail.com',
            'username' => 'senyr@gmail.com',
            'password' => bcrypt('passer13'),
        ]);

        DB::table("users")->insert([
            'name' => "aminata",
            'role' => "user",
            'email' => 'aminata@gmail.com',
            'username' => 'senyr@gmail.com',
            'password' => bcrypt('passer13'),
        ]);
    }
}
