@extends("layout")

@section("content")
    <h1>Hello world !</h1>

    <ul>
        @foreach($students as $student)
            <li>
                @if($student != "Alassane")
                {{ $student }} est un étudiant
                    @else
                    {{ $student }} <b style="color: darkgreen">est l'assistant</b>
                    @endif
            </li>
        @endforeach
    </ul>
@endsection
