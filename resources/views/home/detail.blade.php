@extends('layout')

@section("slideshow")
@endsection

@section('content')
    <div class="row">
        <div class="col-4">
            <img class="img-thumbnail" src="{{ $post->media }}" alt="">

        </div>
        <div class="col-8">
            <h3>{{ $post->title }}</h3>
            <p>
                {{ $post->content }}
            </p>
        </div>
    </div>
@endsection
