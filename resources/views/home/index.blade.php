@extends('layout')

@section('title')
    Bienvenue
@endsection
@section('content')

    <div class="row">
        <div class="col-8">

            <h3>Liste des derniers articles</h3>
            @foreach($recent_posts as $recent_post)


                <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="{{ $recent_post->media }}" class="card-img" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">{{ $recent_post->title }}</h5>
                                <p class="card-text">
                                    {{ $recent_post->content }}
                                </p>
                                <p class="card-text">
                                    <small class="text-muted">
                                        <a href="{{ route('detail', $recent_post->slug) }}">
                                            Voir plus
                                        </a>
                                    </small>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            @endforeach
        </div>
        <div class="col-4">
            <h3>La liste des catégories</h3>
        </div>
    </div>
@endsection
