@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Titre</label>
                <input name="title" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ex : Macky SAll">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="">Contenu</label>
                <textarea name="content" id="" class="form-control">

                </textarea>
            </div>
            <div class="form-group">
                <label for="">Media</label>
                <input type="file" name="media" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Date de publication</label>
                <input type="datetime-local" class="form-control" name="date_publication">
            </div>

            <div class="form-group form-check">
                <input name="status" type="checkbox" class="form-check-input" id="exampleCheck1">
                <label class="form-check-label" for="exampleCheck1">Statut</label>
            </div>
            <button type="submit" class="btn btn-success">Envoyer</button>
        </form>
    </div>
</div>
@endsection
