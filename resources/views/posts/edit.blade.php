@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <form action="{{ route('posts.update', $post->id) }}" method="post" enctype="multipart/form-data" >
                @csrf
                {{ method_field('PATCH') }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Titre</label>
                    <input value="{{ $post->title }}" name="title" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Ex : Macky SAll">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
                <div class="form-group">
                    <label for="">Contenu</label>
                    <textarea name="content" id="" class="form-control">{{ $post->content }}</textarea>
                </div>
                <div class="form-group">
                    <label for="">Media</label>
                    <img style="height: 40px;" src="{{ asset($post->media) }}" alt="">
                    <input type="file" name="media" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Date de publication</label>
                    <input value="{{ $post->date_publication }}" type="datetime-local" class="form-control" name="date_publication">
                </div>

                <div class="form-group form-check">
                    <input @if($post->status == 1) checked @endif name="status" type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label  class="form-check-label" for="exampleCheck1">Statut</label>
                </div>
                <button type="submit" class="btn btn-success">Envoyer</button>
            </form>
        </div>
    </div>
@endsection
