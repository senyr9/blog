@extends('layouts.app')

@section('content')
    <div class="container">
        @if(Session()->get('success'))
            <label class="alert alert-success">
                {{ Session()->get('success') }}
            </label>
        @endif
        <a href="{{ route('posts.create') }}" class="btn btn-primary">
            Ajouter un nouveau
        </a>
        <div class="row col-12">
            <table class="table">
                <thead>
                <tr>
                    <th>Titre</th>
                    <th>Date de publication</th>
                    <th>Actions</th>
                </tr>
                </thead>

                <tbody>
                @foreach($posts as $post)
                    @if($post->status == 1)
                    <tr>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->date_publication }}</td>
                        <td>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary">View</button>
                                <a href="{{ route("posts.edit", $post->id) }}" type="button" class="btn btn-warning">Edit</a>

                                <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                                    @csrf
                                    {{ method_field('DELETE') }}
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                    @endif
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
