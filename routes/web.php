<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home')->name('home');

Route::get('/detail/{slug}', 'HomeController@detail')->name('detail');

Route::get('/hello/{student}', "HelloController@index")->name('hello.name');
Route::get('/hello', "HelloController@hello")->name('hello');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/posts', 'PostController') ;
});


Auth::routes();

Route::get('/admin', 'HomeController@index')->name('admin');
